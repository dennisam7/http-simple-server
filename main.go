//go:build ignore

package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	// Init Multiplexer Router
	r := mux.NewRouter()

	// Endpoints
	r.HandleFunc("/", HandleAddDataTask).Methods("POST")
	r.HandleFunc("/", HandleGetStattisticsTask).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", r))

}
