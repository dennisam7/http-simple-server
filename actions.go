//go:build ignore

package main // consider create it by action package

import (
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// consider create safe map by yourself, DON'T USE safe.map that's BIG NO NO !
var DB = make(map[string]int) // map db
var mutex = &sync.Mutex{}     // map db isn't async safty, I created a lock

func HandleGetStattisticsTask(w http.ResponseWriter, r *http.Request) {
	/*
		Input: writer, request
		This function calculate Stattistcs and return the results.
		Stattistcs are top5 names, least name and the median.

		Note: this is an handler so whenever this function will be called,
		gorilla/mux worreid that it will be auto goroutine already.
	*/
	if len(DB) < 5 {
		// if there no 5 names in the "DB" return statusForbidden
		w.WriteHeader(http.StatusForbidden)
		return
	}
	mutex.Lock()
	data := GetStattistics() // consider: goroutine this function
	mutex.Unlock()
	w.Write([]byte(data))
}

func HandleAddDataTask(w http.ResponseWriter, r *http.Request) {
	/*
		Input: writer, request
		This function gets name seperated by comma on http body request.
		This function insert the names on DB.

		Note: this is an handler so whenever this function will be called,
		gorilla/mux worreid that it will be auto goroutine already.
	*/
	if data, err := ioutil.ReadAll(r.Body); err == nil {
		if len(data) == 0 { // if bad input, send bad request
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		mutex.Lock()
		AddData(string(data))
		mutex.Unlock()
		w.WriteHeader(http.StatusCreated) // 201 :) HTTP
	} else {
		w.WriteHeader(http.StatusBadRequest) // 400 :(
	}

}

// This function calculate stattistcs and return it by string
func GetStattistics() string {

	stattistics := "top5:\n"
	keys := make([]string, 0, len(DB))

	for k := range DB {
		keys = append(keys, k)
	}
	// consider: make sorted map when insert data. maybe it will save a lot of time in future. maybe not.
	sort.SliceStable(keys, func(i, j int) bool {
		return DB[keys[i]] > DB[keys[j]]
	})
	// top 5 names
	for i := 0; i < 5; i++ {
		stattistics = stattistics + keys[i] + " " + strconv.Itoa(DB[keys[i]]) + "\n"
	}
	// least
	stattistics = stattistics + "\n" + "leaset: " + strconv.Itoa(DB[keys[len(DB)-1]]) + "\n\n"

	// median
	// Notice: DB must be bigger than 2 this function called by `HandleGetStattisticsTask` so it checked it already.
	median := (DB[keys[int(len(DB)/2)-1]] + DB[keys[int(len(DB)/2)]]) / 2
	return stattistics + "median: " + strconv.Itoa(median)
}

// This function recive seperated data by string, the function seperate the data and insert to map DB.
func AddData(data string) {
	persons := strings.Split(data, ",")

	for i := 0; i < len(persons); i++ {
		// if persion exist, add his count
		if val, ok := DB[persons[i]]; ok {
			DB[persons[i]] = val + 1
		} else {
			// if not exist create a person
			DB[persons[i]] = 1
		}
	}
}
